package tp10_StateAndStrategy_ej01_Encripter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import tp10_StateAndStrategy_ej01_Encripter.EncripterNaive;
import tp10_StateAndStrategy_ej01_Encripter.StringDesorder;
import tp10_StateAndStrategy_ej01_Encripter.ToNumbers;
import tp10_StateAndStrategy_ej01_Encripter.VocalNext;



class EncripterNaiveTest {
	
	EncripterNaive encripter;
	String mensaje = "abc def ghi jkl";
	
	
	
	@BeforeEach
	void setUp()
	{
		encripter = new EncripterNaive();
	}
	
	@Test
	void testStringDesorderEncriptar() {
		String nuevoMensaje = "acbde fghij klmn� opqrs tuvwx yz";
		encripter.cambiarModo(new StringDesorder());
		assertEquals("klmn� acbde fghij yz opqrs tuvwx", encripter.encriptar(nuevoMensaje));
	}
	
	@Test
	void testStringDesorderDesencriptar() {
		encripter.cambiarModo(new StringDesorder());
		String mensajeEncriptado = "klmn� acbde fghij yz opqrs tuvwx";
		
		assertEquals("acbde fghij klmn� opqrs tuvwx yz", encripter.desencriptar(mensajeEncriptado));
	}
	
	@Test
	void testStringDesorderEncriptarCaso2() {
		String nuevoMensaje = "eran los mejores tiempos eran los peores tiempos";
		encripter.cambiarModo(new StringDesorder());
		assertEquals("mejores eran los los tiempos eran peores tiempos", encripter.encriptar(nuevoMensaje));
	}
	
	@Test
	void testStringDesorderDesencriptarCaso2() {
		encripter.cambiarModo(new StringDesorder());
		String mensajeEncriptado = "mejores eran los los tiempos eran peores tiempos";
		
		assertEquals("eran los mejores tiempos eran los peores tiempos", encripter.desencriptar(mensajeEncriptado));
	}
	
	


	@Test
	void testVocalNextEncriptar() {
		String nuevoMensaje = "ahh querido murcielago seras lo que tengas que ser o no seras nada";
		encripter.cambiarModo(new VocalNext());
		
		assertEquals("ehh qairodu marcoilegu sires lu qai tinges qai sir u nu sires nede", encripter.encriptar(nuevoMensaje));
	}
	
	@Test
	void testVocalNextDesencriptar() {
		encripter.cambiarModo(new VocalNext());
		String mensajeEncriptado = "ehh qairodu marcoilegu sires lu qai tinges qai sir u nu sires nede";
		
		assertEquals("ahh querido murcielago seras lo que tengas que ser o no seras nada", encripter.desencriptar(mensajeEncriptado));
	}
	
	@Test
	void testToNumbersEncriptar() {
		String nuevoMensaje = "acbde fghij klmn� opqrs tuvwx yz";
		encripter.cambiarModo(new ToNumbers());
		
		assertEquals("1,3,2,4,5, 6,7,8,9,10, 11,12,13,14,15, 16,17,18,19,20, 21,22,23,24,25, 26,27,", encripter.encriptar(nuevoMensaje));
	}
	
	@Test
	void testToNumbersDesencriptar() {
		encripter.cambiarModo(new ToNumbers());
		String mensajeEncriptado = "1,3,2,4,5, 6,7,8,9,10, 11,12,13,14,15, 16,17,18,19,20, 21,22,23,24,25, 26,27,";
		
		assertEquals("acbde fghij klmn� opqrs tuvwx yz", encripter.desencriptar(mensajeEncriptado));
	}

}
