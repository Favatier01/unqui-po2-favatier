package Ejercicio02;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ProductoCooperaticaTest {
	
	public ProductoCooperativa leche;
	
	@BeforeEach
	public void setUp() {
		
		leche = new ProductoCooperativa("Leche",100, 1);
		
	}
	
	@Test
	public void constuctorTest() {
		assertEquals("Leche", leche.getNombre());
		assertEquals(90, leche.getPrecio());
	}
	

	

}
