package Ejercicio02;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class StockeableTest {
	
	public Stockeable zapatilla;
	
	@BeforeEach
	public void setUp()
	{
		zapatilla = new Stockeable(new Producto("Zapatilla topper air", 1000, 1), 50);
		
	}
	
	
	@Test
	public void constructorTest() {
		assertEquals("Zapatilla topper air", zapatilla.getNombre());
		assertEquals(50, zapatilla.getCantidad());
	}

	@Test
	public void aumentarCantidadTest()
	{
		zapatilla.aumentarStock(53);
		assertEquals(103, zapatilla.getCantidad());
		zapatilla.aumentarStock(1);
		assertEquals(104, zapatilla.getCantidad());
	}
	
	@Test
	public void disminuirCantidadDeStock() {
		zapatilla.disminuirStock(10);
		assertEquals(40, zapatilla.getCantidad());
	}
}
