package Ejercicio02;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class StockTest {

	
	public Stock stock;
	
	Producto zapatilla;
	Producto campera;
	Producto leche;
	
	Stockeable zapatillas;
	Stockeable camperas;
	Stockeable leches;
	
	@BeforeEach
	public void setUp() {
		stock = new Stock();
		
		zapatilla = new Producto("Nique", 1000,  1);
		campera   = new Producto("bossT",20000, 1);
		leche     = new ProductoCooperativa("Leche", 100, 1);
		
		zapatillas = new Stockeable(zapatilla, 1000);
		camperas   = new Stockeable(campera, 10);
		leches     = new Stockeable(leche, 100);
		
		stock.agregarProducto(zapatillas);
		stock.agregarProducto(camperas);
		stock.agregarProducto(leches);
		
		
	}
	
	
	
	public void agregarYSacarProductoTest()
	{
		assertEquals(0, stock.cantidadDeProductosStockeables());
		
		stock.agregarProducto(zapatillas);
		stock.agregarProducto(camperas);
		stock.agregarProducto(leches);
		
		assertEquals(3, stock.cantidadDeProductosStockeables());
		
		stock.sacarDeStock(camperas);
		
		assertEquals(2, stock.cantidadDeProductosStockeables());
	}
	
	@Test
	public void aumentarStockTest()
	{
		stock.aumentarStock(zapatillas, 111);
		assertEquals(1111, stock.consultarStock("Nique"));
		
		stock.aumentarStock(camperas, 1);
		assertEquals(11, stock.consultarStock("bossT"));
		
		stock.aumentarStock(leches, 111);
		assertEquals(211, stock.consultarStock("Leche"));
	}
	
	
	@Test
	public void disminuirTest()
	{
		stock.disminuirStock(zapatillas, 1);
		assertEquals(999, stock.consultarStock("Nique"));
		
		stock.disminuirStock(camperas, 1);
		assertEquals(9, stock.consultarStock("bossT"));
		
		stock.disminuirStock(leches, 1);
		assertEquals(99, stock.consultarStock("Leche"));
	}
}
