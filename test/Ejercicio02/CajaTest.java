package Ejercicio02;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.*;


public class CajaTest {
	
	Caja caja;
	
	Producto zapatilla;
	Producto campera;
	Producto tomate;
	
	Servicio telefonica;
	Impuesto segba;
	
	@BeforeEach
	public void setUp()
	{
		zapatilla = new Producto("Nique", 2500, 50);
		campera   = new Producto("BossT", 10000, 30);
		tomate    = new ProductoCooperativa("Tomate Perita", 100, 100);
		
		telefonica = new Servicio(100, 1);
		segba      = new Impuesto(100);
		
		
		caja = new Caja();
		caja.agregarProducto(zapatilla);
		caja.agregarProducto(campera);
		caja.agregarProducto(tomate);
		caja.agregarProducto(telefonica);
		caja.agregarProducto(segba);
	}
	
	
	@Test
	public void totalTest() {
		
		float total = 2500 + 10000 + 90 + 100 + 100;
		assertEquals(total, caja.costoTotal());
	}
	

	
}
