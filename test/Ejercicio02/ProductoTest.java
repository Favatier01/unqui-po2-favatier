package Ejercicio02;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class ProductoTest {

	Producto arroz;

	@BeforeEach
	void setup() 
	{
		arroz = new Producto("Arroz", 200, 1);
		
	}

	@Test
	public void contructorTest() {
		
		assertEquals("Arroz", arroz.getNombre());
		assertEquals(200, arroz.getPrecio());
	}
	

}
