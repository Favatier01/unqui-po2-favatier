package testAndDoubles01Poker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class PokerStatusTest {

	
	PokerStatus pokerStatus;
	
	@BeforeEach
	void setUp()
	{
		pokerStatus = new PokerStatus();
	}
	
	
	//Escenario 1. Todas las cartas son diferentes por lo tanto no debe haber poker.
	@Test
	void verificarTest()
	{
		PokerStatus.verificar("1P", "2P", "3");
	}
}
