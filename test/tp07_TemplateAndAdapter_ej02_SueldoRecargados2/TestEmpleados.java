package tp07_TemplateAndAdapter_ej02_SueldoRecargados2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import tp07_TemplateAndAdapter_ej02_SueldoRecargados.Pasante;
import tp07_TemplateAndAdapter_ej02_SueldoRecargados.Planta;
import tp07_TemplateAndAdapter_ej02_SueldoRecargados.Temporario;

class TestEmpleados {

	@Test
	void testTemporario() {
		Temporario temporario = new Temporario(1000, 5, 3, true);
		temporario.setHorasTrabajadas(1);
		
		
		assertEquals(1105 * 0.87f, temporario.calcularSueldo());
	}
	
	@Test
	void testPasante() {
		Pasante pasante = new Pasante(40);
		pasante.setHorasTrabajadas(1);
		
		assertEquals(40 * 0.87f, pasante.calcularSueldo());
	}
	
	@Test
	void testPlanta() {
		Planta planta = new Planta(3000, 150, 1);
		planta.setHorasTrabajadas(1);
		
		assertEquals(3150 * 0.87f, planta.calcularSueldo());
	}

}
