package tp10_StateAndStrategy_ej03_Mp3;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

import org.junit.jupiter.api.BeforeEach;

public class TestReproductor {

	Reproductor mp3;
	
	@BeforeEach
	void setUp()
	{
		mp3 = new Reproductor(new SeleccionDeCancion());
		
		Song dolina = new Song();
		mp3.agregarCancion(dolina);
		
	}
	
	
	//MODO SELECCION DE CANCIONES
	@Test
	void testPlayEnModoSeleccionDeCancion()
	{
		assertEquals("Reproduciendo: El vals del duende", mp3.botonPlay());
	}
	
	@Test
	void testPauseEnModoSeleccionDeCancion()
	{
		assertThrows(IllegalArgumentException.class,
				()-> mp3.botonPause());
	}
	
	@Test
	void testStopEnModoSeleccionDeCanciones()
	{
		assertEquals("Empty", mp3.botonStop());
	}
	
	
	//MODO REPRODUCCION
	@Test
	void testPlayEnModoReproduccion()
	{
		mp3.botonPlay();
		assertThrows(IllegalArgumentException.class,
				()-> mp3.botonPlay());
	}
	
	@Test
	void testPauseEnModoReproduccion()
	{
		mp3.botonPlay();
		assertEquals("En Pausa: El vals del duende", mp3.botonPause());
	}
	
	@Test
	void testStopEnModoReproduccion()
	{
		mp3.botonPlay();
		assertEquals("Stop: El vals del duende", mp3.botonStop());
		//verifica que este en modo seleccion de canciones que el unico con la funcionalidad play
		assertEquals("Reproduciendo: El vals del duende", mp3.botonPlay());
	}
	
	
	//MODO PAUSADO
	
	@Test
	void apretarPausaCuandoLaCancionEstaPausada()
	{
		mp3.botonPlay();
		mp3.botonPause();
		
		assertEquals("Reproduciendo: El vals del duende", mp3.botonPause());
	}
	
	@Test
	void apretarPlayEnModoPausado()
	{
		mp3.botonPlay();
		mp3.botonPause();
		assertThrows(IllegalArgumentException.class,
				()-> mp3.botonPlay());
	}
	
	@Test
	void apretarStopEnModoPausa()
	{
		mp3.botonPlay();
		mp3.botonPause();
		assertEquals("Stop: El vals del duende", mp3.botonStop());
		//verifica que este en modo seleccion de canciones que el unico con la funcionalidad play
		assertEquals("Reproduciendo: El vals del duende", mp3.botonPlay());
	}
}
