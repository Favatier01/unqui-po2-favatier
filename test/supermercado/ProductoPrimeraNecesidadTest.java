package supermercado;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ProductoPrimeraNecesidadTest {

	private ProductoDePrimeraNecesidad leche;
	
	@BeforeEach
	public void setUp() {
		leche = new ProductoDePrimeraNecesidad("Leche", 100d, false, 25);
	}
	
	@Test
	public void testCalcularPrecio() {
		assertEquals(new Double(75), leche.getPrecio());
	}
}
