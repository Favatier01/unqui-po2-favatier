package tp07_TemplateAndAdapter_ej02_SueldoRecargados3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestCaseEmpleado {

	@Test
	void testTemporario() {
		Temporario temp = new Temporario(true);
		temp.setHorasTrabajadas(1);
		
		assertEquals(1105 * 0.87f, temp.calcularSueldo());
	}
	
	
	@Test
	void testPlanta() {
		Planta planta = new Planta();
		planta.setCantidadDeHijos(1);
		assertEquals(3150 * 0.87f, planta.calcularSueldo());
		
	}
	
	
	@Test
	void testPasante() {
		Pasante pasante = new Pasante();
		pasante.setHorasTrabajadas(1);
		
		assertEquals(40 * 0.87f, pasante.calcularSueldo());
	}

}
