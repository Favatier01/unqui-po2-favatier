package tp10_StateAndStrategy_ej02_videoJuego;


import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.BeforeEach;



class VideoGameTest {

	VideoJuego game;
	
	@BeforeEach
	void setUp() {
		
		game = new VideoJuego();
	}

	@Test
	void apretarBotonCuandoNoHayFichas()
	{
		
		assertEquals("Pantalla", game.presionarBoton());
	}
	
	@Test
	void apretarBotonCuandoHayUnaFicha()
	{
		game.agregarFicha();
		game.actualizarEstadoMaquina();

		assertEquals("Juego iniciado para un jugador", game.presionarBoton());
		
	}
	
	@Test
	void apretarBotonCuandoSeEstaJugando()
	{
		game.agregarFicha();
		game.actualizarEstadoMaquina();
		game.presionarBoton();
		
		assertEquals("Boton apagado", game.presionarBoton());
		
	}
	
	@Test
	void presionarBotonCuandoMasDeUnaFicha()
	{
		game.agregarFicha();
		game.agregarFicha();
		game.actualizarEstadoMaquina();
		
		assertEquals("Juego iniciado para dos jugadores", game.presionarBoton());
		assertEquals("Boton apagado", game.presionarBoton());
	}
	
	@Test
	void comportamientoAlFinalizarElJuego()
	{
		game.agregarFicha();
		game.actualizarEstadoMaquina();
		game.presionarBoton();
		game.finalizarJuego();
		assertEquals("Pantalla", game.presionarBoton());
		
		
	}

}
