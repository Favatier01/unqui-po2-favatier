package unq;


//import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CounterTestCase {

	
		private Counter counter;
		
		@BeforeEach
		public void setUp() throws Exception {
			counter = new Counter();
			
			counter.addNum(1);
			counter.addNum(3);
			counter.addNum(5);
			counter.addNum(7);
			counter.addNum(9);
			counter.addNum(1);
			counter.addNum(1);
			counter.addNum(1);
			counter.addNum(1);
			counter.addNum(4);
		}
		
		
		@Test
		void testEvenNumbers()
		{
			int amount = counter.contarPares();
			
			assertEquals(amount, 1);		
		}
	}
	

	
	
	


