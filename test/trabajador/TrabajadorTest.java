package trabajador;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;



public class TrabajadorTest {
	Trabajador unTrabajador;

	
	@BeforeEach
	public void setUp() {
		unTrabajador = new Trabajador();
		unTrabajador.agregarIngreso(new Ingreso(100000, 1, "Programacion", 15));
		unTrabajador.agregarIngreso(new Ingreso(100000, 2, "Programacion", 10));
		unTrabajador.agregarIngreso(new Ingreso(100000, 3, "Programacion", 5));
		unTrabajador.agregarIngreso(new Ingreso(100000, 4, "Programacion", 0));
			
			
	}
	
	@Test
	public void test() {
		assertEquals(400000f, unTrabajador.getMontoImponible());
		assertEquals((400000*0.02), unTrabajador.getImpuestoAPagar());
		assertEquals((400000 + 100*(15+10+5)), unTrabajador.getTotalPercibido());
	}
	

}
