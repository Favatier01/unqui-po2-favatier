package trabajador;


import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;



public class IngresoTest {
	
	Ingreso unIngreso;
	
	@BeforeEach
	public void setUp() {
		
		unIngreso = new Ingreso(100000, 1, "Programacion", 15);

			
			
	}
	
	@Test
	public void constructorTest() {
		assertEquals(100000, unIngreso.getIngreso());
		assertEquals(1, unIngreso.getMes());
		assertEquals("Programacion", unIngreso.getConcepto());
		assertEquals(15, unIngreso.getHorasExtra()); 
	}
	
	

}

