package Ejercicio02;

public class Stockeable {

	private Producto producto;
	private int cantidad;
	
	public Stockeable(Producto unProducto, int unaCantidad)
	{
		this.setProducto(unProducto);
		this.setCantidad(unaCantidad);
	}

	
	//METODOS
	//Los metodos implementados a continuacion tienen el siguiente defecto
	//que pasaria si hay alguna mal gestion y por ejemplo se resta mas stock del que hay quedando una cantidad negativa?
	public void disminuirStock(int unaCantidad) 
	{
		if(unaCantidad > 0)
		{
			this.cambiarCantidad(-unaCantidad);			
		}
		else
		{
			System.out.println("La cantidad ingresada debe ser positiva");
		}

	}
	
	
	public void aumentarStock(int unaCantidad)
	{
		cambiarCantidad(unaCantidad);
	}
	
	
	private void cambiarCantidad(int unaCantidad)
	{
		int nuevaCantidad = this.getCantidad() + unaCantidad;
		this.setCantidad(nuevaCantidad);
	}
	

	
	
	public String getNombre()
	{
		return getProducto().getNombre();
	}
	
	
	
	
	//GETTERS AND SETTERS
	private Producto getProducto() {
		return producto;
	}

	private void setProducto(Producto producto) {
		this.producto = producto;
	}

	public int getCantidad() {
		return cantidad;
	}

	private void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
	
	
}
