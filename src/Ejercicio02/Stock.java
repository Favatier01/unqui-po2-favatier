package Ejercicio02;

import java.util.ArrayList;

public class Stock {
	private ArrayList<Stockeable> productosEnStock = new ArrayList<>();


	
	public void aumentarStock(Stockeable productoDeStock, int unaCantidad) {
		
		productoDeStock.aumentarStock(unaCantidad);
		
			
	}
	
	
	public void disminuirStock(Stockeable productoDeStock, int unaCantidad)
	{
		productoDeStock.disminuirStock(unaCantidad);
	}

	public void agregarProducto(Stockeable unProductoDeStock)
	{
		productosEnStock.add(unProductoDeStock);
	}

	
	public void sacarDeStock(Stockeable unProductoDeStock)
	{
		productosEnStock.remove(unProductoDeStock);
	}
	
	
	public int cantidadDeProductosStockeables()
	{
		return productosEnStock.size();
	}
	
	public int consultarStock(String unNombre)
	{
		int index = 0;
		
		for(Stockeable unProductoStockeable:productosEnStock)
		{
			
			if(unProductoStockeable.getNombre() == unNombre)
			{
				break;
			}
			
			index += 1;
		}
		
		return productosEnStock.get(index).getCantidad();
	}

}