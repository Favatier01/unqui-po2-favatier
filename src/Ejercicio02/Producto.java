package Ejercicio02;

import java.math.*;

public class Producto implements Cobrables{
	
	private String nombre;
	private float precio;
	private int stock;
	
	

	public void agregarStock(int unaCantidad)
	{
		int nuevaCantidad = this.getStock() +  Math.abs(unaCantidad); 
		this.setStock(nuevaCantidad);
	}
	
	
	public void quitarStock(int unaCantidad)
	
	{
		int nuevaCantidad = this.getStock() - Math.abs(unaCantidad);
		this.setStock(nuevaCantidad);
	}
	
	


	public Producto(String unNombre, float unPrecio, int unaCantidad)
	{
		this.setNombre(unNombre);
		this.setPrecio(unPrecio);
		this.setStock(unaCantidad);
	}
	
	
	public String getNombre() 
	{
		return nombre;
	}
	
	
	public void setNombre(String nombre) 
	{
		this.nombre = nombre;
	}
	
	
	public float getPrecio() 
	{
		return precio;
	}

	
	public void setPrecio(float precio) 
	{
		this.precio = precio;
	}
	
	private int getStock() {
		return stock;
	}


	private void setStock(int stock) {
		this.stock = stock;
	}

}