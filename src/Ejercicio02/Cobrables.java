package Ejercicio02;

public interface Cobrables {
	
	public float getPrecio();
	public void quitarStock(int unaCantidad);

}
