package Ejercicio02;

public class Servicio implements Cobrables
{
	private int unidadesConsumidas;
	private float costoPorUnidad;
	
	
	public Servicio(int unasUnidadesConsumidas, float costoPorUnidad)
	{
		this.setCostoPorUnidad(costoPorUnidad);
		this.setUnidadesConsumidas(unasUnidadesConsumidas);
	}
	
	
	public float calculatTotal() {
		return this.getCostoPorUnidad()*this.getUnidadesConsumidas();
	}
	
	
	
	private int getUnidadesConsumidas() {
		return unidadesConsumidas;
	}
	private void setUnidadesConsumidas(int unidadesConsumidas) {
		this.unidadesConsumidas = unidadesConsumidas;
	}
	private float getCostoPorUnidad() {
		return costoPorUnidad;
	}
	private void setCostoPorUnidad(float costoPorUnidad) {
		this.costoPorUnidad = costoPorUnidad;
	}


	@Override
	public float getPrecio() {
		
		return this.calculatTotal();
	}


	@Override
	public void quitarStock(int unaCantidad) {
		// TODO Auto-generated method stub
		
	}
	
	
	

}
