package Ejercicio02;

import java.util.ArrayList;

public class Caja  {
	
	private ArrayList<Cobrables> productos = new ArrayList<Cobrables>();
	//private float totalAPagar;
	
	private ArrayList<Cobrables> getProductos(){
		return productos;
	}
	
	public void agregarProducto(Cobrables unProducto)
	{
		this.getProductos().add(unProducto);
		unProducto.quitarStock(1);
	}
	
	public float costoTotal()
	{
		float total = 0f;
		
		for(Cobrables producto:productos)
		{
			total += producto.getPrecio();
		}
		
		return total;
	}

}
