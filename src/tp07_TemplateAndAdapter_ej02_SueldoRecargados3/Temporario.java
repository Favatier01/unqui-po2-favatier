package tp07_TemplateAndAdapter_ej02_SueldoRecargados3;

public class Temporario extends Empleado{

	
	float sueldoBasico;
	float horasTrabajadas;
	float sueldoPorHora;
	boolean tieneFamilia;
	float sueldoPorFamilia;
	

	public Temporario(boolean tieneFamilia) {
		this.setSueldoBasico(1000);
		this.setSueldoPorHora(5);
		this.setSueldoPorFamilia(100);
		this.setTieneFamilia(tieneFamilia); 
	}

	

	public float getSueldoPorFamilia() {
		return sueldoPorFamilia;
	}



	public void setSueldoPorFamilia(float sueldoPorFamilia) {
		this.sueldoPorFamilia = sueldoPorFamilia;
	}



	public boolean isTieneFamilia() {
		return tieneFamilia;
	}



	public void setTieneFamilia(boolean tieneFamilia) {
		this.tieneFamilia = tieneFamilia;
	}



	public float getSueldoBasico() {
		return sueldoBasico;
	}



	public void setSueldoBasico(float sueldoBasico) {
		this.sueldoBasico = sueldoBasico;
	}



	public float getHorasTrabajadas() {
		return horasTrabajadas;
	}



	public void setHorasTrabajadas(float horasTrabajadas) {
		this.horasTrabajadas = horasTrabajadas;
	}



	public float getSueldoPorHora() {
		return sueldoPorHora;
	}



	public void setSueldoPorHora(float sueldoPorHora) {
		this.sueldoPorHora = sueldoPorHora;
	}



	@Override
	public float doSueldoBasico()
	{
		return this.sueldoBasico;
	}
	
	
	@Override
	public float doRetribucionPorHorasTrabajadas()
	{
		return this.getSueldoPorHora()*this.getHorasTrabajadas();
		
	}
	
	
	@Override
	public float doRetribucionPorFamilia() {

		float sueldoPorFamilia = 0f;
		
		if(this.isTieneFamilia())
		{
			sueldoPorFamilia += this.getSueldoPorFamilia();
		}
		
		return sueldoPorFamilia;
	}
	


}
