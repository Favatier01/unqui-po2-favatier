package tp07_TemplateAndAdapter_ej02_SueldoRecargados3;

public class Pasante extends Empleado{

	private float horasTrabajadas;
	private float retribucionPorHora;
	
	
	public Pasante() {
		super();
		this.setRetribucionPorHora(40f);
	}

	public float getHorasTrabajadas() {
		return horasTrabajadas;
	}

	public void setHorasTrabajadas(float horasTrabajadas) {
		this.horasTrabajadas = horasTrabajadas;
	}

	public float getRetribucionPorHora() {
		return retribucionPorHora;
	}

	public void setRetribucionPorHora(float retribucionPorHora) {
		this.retribucionPorHora = retribucionPorHora;
	}



	@Override
	public float doRetribucionPorHorasTrabajadas() {
		// TODO Auto-generated method stub
		return this.getHorasTrabajadas()*this.getRetribucionPorHora();
	}






}
