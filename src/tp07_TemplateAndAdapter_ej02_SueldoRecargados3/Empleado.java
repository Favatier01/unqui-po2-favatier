package tp07_TemplateAndAdapter_ej02_SueldoRecargados3;

public abstract class Empleado {
	

		
	public float calcularSueldo()
	{
		float sueldoBruto = 0f;
		float sueldoBasico = this.doSueldoBasico();
		float sueldoPorHorasTrabajadas = this.doRetribucionPorHorasTrabajadas();
		float sueldoPorFamilia = this.doRetribucionPorFamilia();
		
		
		sueldoBruto += sueldoBasico + sueldoPorHorasTrabajadas + sueldoPorFamilia;
		
		
		return sueldoBruto - this.calcularDescuentos(sueldoBruto);

	}
	
	public  float doSueldoBasico()
	{
		return 0;
	}
	
	public float doRetribucionPorHorasTrabajadas( )
	{
		return 0;
	}
	
	public  float doRetribucionPorFamilia()
	{
		return 0;
	}

	public float calcularDescuentos(float sueldoBruto)
	{
		return 0.13f * sueldoBruto;
	}
	
	
}
