package tp07_TemplateAndAdapter_ej02_SueldoRecargados3;

public class Planta extends Empleado{

	int cantidadDeHijos;
	float sueldoPorCadaHijo;
	float sueldoBasico;
	
	
	public Planta() {
		this.setSueldoPorCadaHijo(150);
		this.setSueldoBasico(3000);
	}
	
	
	
	public float getSueldoBasico() {
		return sueldoBasico;
	}



	public void setSueldoBasico(float sueldoBasico) {
		this.sueldoBasico = sueldoBasico;
	}



	public int getCantidadDeHijos() {
		return cantidadDeHijos;
	}

	public void setCantidadDeHijos(int cantidadDeHijos) {
		this.cantidadDeHijos = cantidadDeHijos;
	}

	
	
	public float getSueldoPorCadaHijo() {
		return sueldoPorCadaHijo;
	}

	public void setSueldoPorCadaHijo(float sueldoPorCadaHijo) {
		this.sueldoPorCadaHijo = sueldoPorCadaHijo;
	}

	
	@Override
	public float doSueldoBasico()
	{
		return this.getSueldoBasico();
	}
	@Override
	public float doRetribucionPorFamilia() {
		
		return this.getSueldoPorCadaHijo() * this.getCantidadDeHijos();
	}

}
