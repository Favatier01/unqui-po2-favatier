package tp07_TemplateAndAdapter_ej05_AyudandoAlSoberano;

public class CajaDeAhorro extends CuentaBancaria {
	
	private int limite ;

	public CajaDeAhorro(String titular , int limite ){
		super ( titular );
		this . limite = limite ;
	}
	
	public int getLimite(){
		return this . limite ;
	}

	@Override
	public boolean tienePermitidaLaExtraccion(int monto) {
		
		boolean puedeExtraer = false;
		
		if( this.getSaldo()>= monto && this.getLimite()>= monto)
		{
			puedeExtraer = true;
		}
		
		return puedeExtraer;
	}


	
	
	
	/*
	@Override
	public void extraer( int monto ) {
		if ( this.getSaldo()>= monto && this .getLimite()>= monto ){
			this.setSaldo( this .getSaldo()- monto );
			this.agregarMovimientos( "Extraccion" );
		}
	}
	*/
}