package tp07_TemplateAndAdapter_ej05_AyudandoAlSoberano;

import java.util.ArrayList;
import java.util.List;

public abstract class CuentaBancaria {
	

	private String titular ;
	private int saldo ;
	private List<String> movimientos;
	
	public CuentaBancaria(String titular )
	{
		this . titular = titular ;
		this . saldo =0;
		this . movimientos = new ArrayList<String>();
	}
	
	public String getTitular(){
		return this . titular ;
	}
	
	public int getSaldo(){
		return this . saldo ;
	}
	
	protected void setSaldo( int monto ){
		this . saldo = monto ;
	}
	
	public void agregarMovimientos(String movimiento ){
		this . movimientos .add( movimiento );
	}
	
	//PARTRE MODIFICADA
	public final void extraer( int monto )
	{
		boolean tienePermitidaLaExtraccion = this.tienePermitidaLaExtraccion(monto);
		
		if(tienePermitidaLaExtraccion)
		{
			this.setSaldo(this.getSaldo() - monto);
			this .agregarMovimientos( "Extraccion" );
		}
	}

	
	public abstract boolean tienePermitidaLaExtraccion(int monto);
}