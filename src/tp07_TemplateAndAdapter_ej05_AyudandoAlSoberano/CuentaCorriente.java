package tp07_TemplateAndAdapter_ej05_AyudandoAlSoberano;



public class CuentaCorriente extends CuentaBancaria {
	
	private int descubierto ;

	public CuentaCorriente(String titular , int descubierto ){
		super( titular );
		this.descubierto = descubierto ;
	}
	
	public int getDescubierto(){
		return this.descubierto ;
	}

	@Override
	public boolean tienePermitidaLaExtraccion(int monto) {
		
		boolean puedeExtraer = false;
		
		if( monto <= this.getSaldo() + this.getDescubierto())
		{
			puedeExtraer = true;
		}
		
		return puedeExtraer;
	}


	
	/*
	@Override
	public void extraer( int monto ) 
	{
	
		if ( this .getSaldo()+ this .getDescubierto()>= monto ){
			this .setSaldo( this .getSaldo()- monto );
			this .agregarMovimientos( "Extraccion" );
		}
	}
	*/
	}
