package deSmalltakAJava;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;


public class Persona {
	LocalDate fechaDeNacimiento;
	String nombre;
	
	DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	LocalDate fechaNac = LocalDate.parse("15/08/1993", formato);
	
	
	public Persona (LocalDate unaFechaDeNacimiento, String unNombre)
	{
		this.setFechaDeNacimiento(unaFechaDeNacimiento);
		this.setNombre(unNombre);
	}
	
	public LocalDate getFechaDeNacimiento() {
		return fechaDeNacimiento;
	}
	
	
	public void setFechaDeNacimiento(LocalDate fechaDeNacimiento) {
		this.fechaDeNacimiento = fechaDeNacimiento;
	}
	
	
	public String getNombre() {
		return nombre;
	}
	
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	/*
	public int edad() {
		
		//ESTO ESTA MAL, EL MES DEVUELVE CUALQUIER COSA ENERO ES 0 Y SE SIGUE INCREMENTANDO. UNA PORQUERIA JAVA
		//ESTO NO FUNCIONA Y NO SE PORQUE PERO LO DEJO ASI PORQUE ME TIENE HINCHADA LAS GINDAS.
		GregorianCalendar today = new GregorianCalendar();
		
		if(today.get(Calendar.YEAR) == this.getFechaDeNacimiento().get(Calendar.YEAR))
		{
			if(today.get(Calendar.MONTH) == this.getFechaDeNacimiento().get(Calendar.MONTH))
			{
				if(today.get(Calendar.DAY_OF_MONTH) >= this.getFechaDeNacimiento().get(Calendar.DAY_OF_MONTH))
				{
					return (today.get(Calendar.YEAR) - this.getFechaDeNacimiento().get(Calendar.YEAR));
				}
				else
				{
					return ((today.get(Calendar.YEAR) - this.getFechaDeNacimiento().get(Calendar.YEAR)) - 1);
				}
			
			}
			else if (today.get(Calendar.MONTH) > this.getFechaDeNacimiento().get(Calendar.MONTH)) 
			{
				return today.get(Calendar.YEAR) - this.getFechaDeNacimiento().get(Calendar.YEAR);
			}
		}
		else if (today.get(Calendar.YEAR) > this.getFechaDeNacimiento().get(Calendar.YEAR))
		{
			
			return today.get(Calendar.YEAR) - this.getFechaDeNacimiento().get(Calendar.YEAR);
		}
		else 
		{
			return ((today.get(Calendar.YEAR) - this.getFechaDeNacimiento().get(Calendar.YEAR)) - 1);
		}
		return 0;
	}
	*/
	


	// 01/01/2000

	LocalDate ahora = LocalDate.now();

	Period periodo = Period.between(fechaNac, ahora);
	System.out.printf("Tu edad es: %s a�os, %s meses y %s d�as",
	                    periodo.getYears(), periodo.getMonths(), periodo.getDays());
	

}
