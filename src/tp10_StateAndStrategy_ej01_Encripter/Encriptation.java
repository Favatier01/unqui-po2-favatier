package tp10_StateAndStrategy_ej01_Encripter;

public abstract class Encriptation {
	
	public abstract String encriptar(String text);
	
	public abstract String desencriptar(String text);

}
