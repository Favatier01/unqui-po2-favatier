package tp10_StateAndStrategy_ej01_Encripter;

//strategy manually, hace lo mismo pero de forma diferente
public class EncripterNaive {

	Encriptation modoDeEncriptacion;
	
	public EncripterNaive()
	{
		this.modoDeEncriptacion = new StringDesorder();
	}
	
	public void cambiarModo(Encriptation unModo)
	{
		this.modoDeEncriptacion = unModo;
	}
	
	
	public String encriptar(String texto)
	{
		return modoDeEncriptacion.encriptar(texto);
	}
	
	public String desencriptar(String texto)
	{
		return modoDeEncriptacion.desencriptar(texto);
	}
}
