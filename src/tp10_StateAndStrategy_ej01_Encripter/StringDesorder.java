package tp10_StateAndStrategy_ej01_Encripter;

public class StringDesorder extends Encriptation{

	// para variar el orden de las palabras se debe modificar el siguiente array
	// Si el texto tiene menor cantidad de palabras que el array, no se encriptara.
	int semilla[] = {2, 0, 1};
	int semillaInversa[] = {1, 2, 0};
	 
	@Override
	public String encriptar(String text) 
	{	
		String palabras[] = text.split(" ");
		String textoEncriptado = "";
		
		int cantidadDeGruposDe3 = palabras.length/semilla.length;
		int cantidadDeGruposProcesados = 0;
		//indexSemilla determina que palabra "aleatoria" es la siguiente.
		int indexSemilla = 0;
		int indexCantidadDePalabras = 0;
		
		while (cantidadDeGruposProcesados < cantidadDeGruposDe3)
		{
			for(int index = 0; index < semilla.length; index++)
			{
				indexSemilla = semilla.length * cantidadDeGruposProcesados + semilla[index];
				textoEncriptado += palabras[indexSemilla] + " ";
				indexCantidadDePalabras++;
			}
			
			cantidadDeGruposProcesados++;
		}
		
		while(indexCantidadDePalabras < palabras.length)
		{
			
			textoEncriptado += palabras[indexCantidadDePalabras] + " ";
			indexCantidadDePalabras++;
		}
		
		return textoEncriptado.substring(0,textoEncriptado.length() -1 );
		
	}

	@Override
	public String desencriptar(String text) {
		
		String palabras[] = text.split(" ");
		String textoDesencriptado = "";
		
		int cantidadDeGruposDe3 = palabras.length/semilla.length;
		int cantidadDeGruposProcesados = 0;
		//indexSemilla determina que palabra "aleatoria" es la siguiente.
		int indexSemilla = 0;
		int indexCantidadDePalabras = 0;
		
		while (cantidadDeGruposProcesados < cantidadDeGruposDe3)
		{
			for(int index = 0; index < semilla.length; index++)
			{
				indexSemilla = semilla.length * cantidadDeGruposProcesados + semillaInversa[index];
				textoDesencriptado += palabras[indexSemilla] + " ";
				indexCantidadDePalabras++;
			}
			
			cantidadDeGruposProcesados++;
		}
		
		while(indexCantidadDePalabras < palabras.length)
		{
			
			textoDesencriptado += palabras[indexCantidadDePalabras] + " ";
			indexCantidadDePalabras++;
		}
		
		return textoDesencriptado.substring(0,textoDesencriptado.length() -1 );
	}

}
