package tp10_StateAndStrategy_ej01_Encripter;

public class VocalNext extends Encriptation{

	@Override
	public String encriptar(String text) {
		String letras[] = text.split("");
		
		int index = 0;
		for(String letra: letras)
		{
			if(letra.equals("a"))
			{
				letras[index] = "e"; 
			}
			else if(letra.equals("e"))
			{
				letras[index] = "i";
			}
			else if(letra.equals("i"))
			{
				letras[index] = "o";
			}
			else if(letra.equals("o"))
			{
				letras[index] = "u";
			}
			else if (letra.equals("u"))
			{
				letras[index] = "a";
			}
			
			index++;
		}
		
		String textoEncriptado = this.reconstruirString(letras);
		
		return textoEncriptado;
	}

	
	private String reconstruirString(String[] letras)
	{
		String textoReconstruido = "";
		for(String letra: letras)
		{
			textoReconstruido += letra;
		}
		
		return textoReconstruido;
	}
	
	
	@Override
	public String desencriptar(String text) {
		String letras[] = text.split("");
		
		int index = 0;
		for(String letra: letras)
		{
			
			if(letra.equals("a"))
			{
				letras[index] = "u";
			}
			else if(letra.equals("u"))
			{
				letras[index] = "o";
			}
			else if(letra.equals("o"))
			{
				letras[index] = "i";
			}
			else if(letra.equals("i"))
			{
				letras[index] = "e";
			}
			else if (letra.equals("e"))
			{
				letras[index] = "a";
			}
			
			index++;
		}
		
		String textoDesencriptado = this.reconstruirString(letras);
		
		return textoDesencriptado;
	}

}
