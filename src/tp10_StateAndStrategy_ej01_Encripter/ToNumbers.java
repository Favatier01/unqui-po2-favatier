package tp10_StateAndStrategy_ej01_Encripter;

public class ToNumbers extends Encriptation{

	@Override
	public String encriptar(String text) {
		
		String letras[] = text.split("");
		
		
		int index = 0;
		for(String letra: letras)
		{
			
			switch (letra) 
			{
				case "a":
					letras[index] = "1,";
					break;
					
				case "b":
					letras[index] = "2,";
					break;
					
				case "c":
					letras[index] = "3,";
					break;
				case "d":
					letras[index] = "4,";
					break;
				case "e":
					letras[index] = "5,";
					break;
				case "f":
					letras[index] = "6,";
					break;
				case "g":
					letras[index] = "7,";
					break;
				case "h":
					letras[index] = "8,";
					break;
				case "i":
					letras[index] = "9,";
					break;
				case "j":
					letras[index] = "10,";
					break;
				case "k":
					letras[index] = "11,";
					break;
				case "l":
					letras[index] = "12,";
					break;
				case "m":
					letras[index] = "13,";
					break;
				case "n":
					letras[index] = "14,";
					break;
				case "�":
					letras[index] = "15,";
					break;
				case "o":
					letras[index] = "16,";
					break;
				case "p":
					letras[index] = "17,";
					break;
				case "q":
					letras[index] = "18,";
					break;
				case "r":
					letras[index] = "19,";
					break;
				case "s":
					letras[index] = "20,";
					break;
				case "t":
					letras[index] = "21,";
					break;
				case "u":
					letras[index] = "22,";
					break;
				case "v":
					letras[index] = "23,";
					break;
				case "w":
					letras[index] = "24,";
					break;
				case "x":
					letras[index] = "25,";
					break;
				case "y":
					letras[index] = "26,";
					break;
				case "z":
					letras[index] = "27,";
					break;
			}
			
			index++;
			
		}
		
		String numeros = this.reconstruirString(letras);
		
		return numeros;
	}

	private String reconstruirString(String[] letras)
	{
		String textoReconstruido = "";
		for(String letra: letras)
		{
			textoReconstruido += letra;
		}
		
		return textoReconstruido;
	}
	
	
	
	@Override
	public String desencriptar(String text) {
		
		
		 String mensajeDesencriptadoCopia = text.replace("10,", "j");
		 String mensajeDesencriptado = mensajeDesencriptadoCopia.replace("11,", "k");
		 mensajeDesencriptadoCopia = mensajeDesencriptado.replace("12,", "l");
		 mensajeDesencriptado = mensajeDesencriptadoCopia.replace("13,", "m");
		 mensajeDesencriptadoCopia = mensajeDesencriptado.replace("14,", "n");
		 mensajeDesencriptado = mensajeDesencriptadoCopia.replace("15,", "�");
		 mensajeDesencriptadoCopia = mensajeDesencriptado.replace("16,", "o");
		 mensajeDesencriptado = mensajeDesencriptadoCopia.replace("17,", "p");
		 mensajeDesencriptadoCopia = mensajeDesencriptado.replace("18,", "q");
		 mensajeDesencriptado = mensajeDesencriptadoCopia.replace("19,", "r");
		 mensajeDesencriptadoCopia = mensajeDesencriptado.replace("20,", "s");
		 mensajeDesencriptado = mensajeDesencriptadoCopia.replace("21,", "t");
		 mensajeDesencriptadoCopia = mensajeDesencriptado.replace("22,", "u");
		 mensajeDesencriptado = mensajeDesencriptadoCopia.replace("23,", "v");
		 mensajeDesencriptadoCopia = mensajeDesencriptado.replace("24,", "w");
		 mensajeDesencriptado = mensajeDesencriptadoCopia.replace("25,", "x");
		 mensajeDesencriptadoCopia = mensajeDesencriptado.replace("26,", "y");
		 mensajeDesencriptado = mensajeDesencriptadoCopia.replace("27,", "z");
		 
		 
		 mensajeDesencriptadoCopia = mensajeDesencriptado.replace("2,", "b");
		 mensajeDesencriptado = mensajeDesencriptadoCopia.replace("3,", "c");
		 mensajeDesencriptadoCopia = mensajeDesencriptado.replace("4,", "d");
		 mensajeDesencriptado = mensajeDesencriptadoCopia.replace("5,", "e");
		 mensajeDesencriptadoCopia = mensajeDesencriptado.replace("6,", "f");
		 mensajeDesencriptado = mensajeDesencriptadoCopia.replace("7,", "g");
		 mensajeDesencriptadoCopia = mensajeDesencriptado.replace("8,", "h");
		 mensajeDesencriptado = mensajeDesencriptadoCopia.replace("9,", "i");
		 mensajeDesencriptadoCopia = mensajeDesencriptado.replace("1,", "a");

		return mensajeDesencriptadoCopia;
	}
	
	/*
	@Override
	public String desencriptar(String text) {
		
		
		
		String letras[] = text.split(",");
		letras = text.split("");
		
		
		int index = 0;
		for(String letra: letras)
		{
			
			
			
			switch (letra) 
			{
				case "1":
					letras[index] = "a";
					break;
				case "2":
					letras[index] = "b";
					break;
				case "3":
					letras[index] = "c";
					break;
				case "4":
					letras[index] = "d";
					break;
				case "5":
					letras[index] = "e";
					break;
				case "6":
					letras[index] = "f";
					break;
				case "7":
					letras[index] = "g";
					break;
				case "8":
					letras[index] = "h";
					break;
				case "9":
					letras[index] = "i";
					break;
				case "10":
					letras[index] = "j";
					break;
				case "11":
					letras[index] = "k";
					break;
				case "12":
					letras[index] = "l";
					break;
				case "13":
					letras[index] = "m";
					break;
				case "14":
					letras[index] = "n";
					break;
				case "15":
					letras[index] = "�";
					break;
				case "16":
					letras[index] = "o";
					break;
				case "17":
					letras[index] = "p";
					break;
				case "18":
					letras[index] = "q";
					break;
				case "19":
					letras[index] = "r";
					break;
				case "20":
					letras[index] = "s";
					break;
				case "21":
					letras[index] = "t";
					break;
				case "22":
					letras[index] = "u";
					break;
				case "23":
					letras[index] = "v";
					break;
				case "24":
					letras[index] = "w";
					break;
				case "25":
					letras[index] = "x";
					break;
				case "26":
					letras[index] = "y";
					break;
				case "27":
					letras[index] = "z";
					break;
				case ",":
					letras[index] = "";
					break;
			}
			
			index++;
		}
		
		String fraseDesencriptada = this.reconstruirString(letras);
		
		return fraseDesencriptada;
	}
*/
	
}
