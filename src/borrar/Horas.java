package borrar;

import java.util.Calendar;

import sem.Tiempo;

public class Horas implements Tiempo{
	
	int horas;
	int minutos;

	
	private void setHora(int unaHora)
	{
		this.horas = unaHora;
	}
	
	private int getHoras()
	{
		return horas;
	}
	
	
	
	private void setMinutos(int unosMinutos)
	{
		this.minutos = unosMinutos;
	}

	
	private int getMinutos()
	{
		return minutos;
	}
	

	@Override
	public int minutosALaFinalizacionDelEstacionamiento() {
			
		int minutosALaFinalizacion = 0;
		
		minutosALaFinalizacion = (this.getHoras() - this.horaActual()) * 60 + (this.getMinutos() - this.minutosActuales());
		
		return minutosALaFinalizacion;

	}
	
	@Override
	public void setHoraDeFinalizacion(int unosMinutos)
	{
		int horas = 0;
		int minutos = unosMinutos;
		
		
		horas = unosMinutos/60;
		
		
	}


	@Override
	public void setHoraAcual() {
		
		
		this.setHora(this.horaActual());
		this.setMinutos(this.minutosActuales());
		
	}
	
	@Override
	public int horaActual()
	{
		Calendar calendario = Calendar.getInstance();
		
		return (calendario.get(Calendar.HOUR_OF_DAY));
	}
	
	@Override
	public int minutosActuales()
	{
		Calendar calendario = Calendar.getInstance();
		
		return (calendario.get(Calendar.MINUTE));
	}
}
