package borrar;

/**
 * 
 * @author Luis Favatier
 *	Con esta interface se hace uso del del princio dependency inversion de SOLID.
 *  de tal forma que la clase estacionamiento no dependa de la forma en que se implementa el tiempo
 *  Lo hice de tal forma porque al no conocer todas las formas de implementar horarios, creo que es muy probable
 *  que cambie de opinion en el futuro e intente hacerlo diferente
 */

public interface Tiempo {
		
	
	
	
	/**
	 * //Devuelve los minutos que faltan para terminar el estacionamiento y si ya ha finalizado devuelve
	 *  cuanto minutos pasaron desde la finalizacion (en numeros negativos). 
	 */
	public int minutosALaFinalizacionDelEstacionamiento();
	
	/**
	 * Guarda la hora y los minutos actuales
	 */
	public void setHoraAcual();
	
	/**
	 * Guarda a que hora finaliza el estacionamiento pasando cuantos minutos faltan.
	 */
	public void setHoraDeFinalizacion(int minutos);

	
	/**
	 * Devuleve la hora acutual en formato 24hs. Ej: si son 14:30 devuelve 14
	 * @return
	 */
	public int horaActual();
	
	/**
	*Devuelve los minutos actuales. Ej: si son 12:45 devuelve 45.
	*/
	public int minutosActuales();
}
