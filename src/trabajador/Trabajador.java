
package trabajador;

import java.util.ArrayList;

public class Trabajador {
	private int dni = 0;
	private ArrayList<Ingreso> ingresos = new ArrayList();
	private float montoImponible = 0;
	private float impuestoAPagar = 0;
	private float totalPercibido = 0;
	

	public float getMontoImponible() {
		return montoImponible;
	}

	public void setMontoImponible(float montoImponible) {
		this.montoImponible = montoImponible;
	}

	public float getTotalPercibido() {
		return totalPercibido;
	}

	public void setTotalPercibido(float totalPercibido) {
		this.totalPercibido = totalPercibido;
	}

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}
	
	
	
	public void agregarIngreso(Ingreso unIngreso)
	{
		montoImponible += unIngreso.getIngreso();
		impuestoAPagar += unIngreso.getIngreso() * 0.02f;
		totalPercibido += unIngreso.getIngreso() + unIngreso.getHorasExtra() * 100;
		ingresos.add(unIngreso);
	}
	
	
	
	public float getImpuestoAPagar()
	{
		return impuestoAPagar;
	}

	public void setImpuestoAPagar(float impuestoAPagar) {
		this.impuestoAPagar = impuestoAPagar;
	}

	public float reCalcularmontoImponible()
	{
		float montoImponible = 0;
		for(Ingreso ingreso:ingresos)
		{
			montoImponible += ingreso.getIngreso();
		}
		
		return montoImponible;
	}
	
	

}
