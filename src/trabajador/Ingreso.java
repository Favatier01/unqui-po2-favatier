package trabajador;

public class Ingreso {
	private float ingreso;
	private int mes;
	private String concepto;
	private float horasExtra;
	private static float precioPorHoraExtra;
	private float ingresoPorHorasExtra = 0;
	
	
	public float getIngreso() {
		return ingreso;
	}
	public void setIngreso(float ingreso) {
		this.ingreso = ingreso;
	}
	public int getMes() {
		return mes;
	}
	public void setMes(int mes) {
		this.mes = mes;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public float getHorasExtra() {
		return horasExtra;
	}
	public void setHorasExtra(float horasExtra) {
		this.horasExtra = horasExtra;
	}
	public static float getPrecioPorHoraExtra() {
		return precioPorHoraExtra;
	}
	public static void setPrecioPorHoraExtra(float precioPorHoraExtra) {
		Ingreso.precioPorHoraExtra = precioPorHoraExtra;
	}
	public float getIngresoPorHorasExtra() {
		return ingresoPorHorasExtra;
	}
	public void setIngresoPorHorasExtra(float ingresoPorHorasExtra) {
		this.ingresoPorHorasExtra = ingresoPorHorasExtra;
	}
	
	public Ingreso(float unMontoBase, int unMes, String unConcepto, float unasHorasExtra)
	{
		//seria mejor usar los setters y getters pero bueno ya lo escribi asi.
		this.ingreso = unMontoBase;
		this.mes     = unMes;
		this.concepto = unConcepto;
		this.horasExtra = unasHorasExtra;
		this.precioPorHoraExtra = 100f;
		
	}
	
	

}
