package supermercado;

public class ProductoDePrimeraNecesidad extends Producto
{
	private double precioDePrimeraNecesidad;
	
	public ProductoDePrimeraNecesidad(String nombre, double precio, double PorcentajeDeDescuento)
	{
		super(nombre, precio);
		if(PorcentajeDeDescuento >= 0 && PorcentajeDeDescuento <= 100)
		{
			precioDePrimeraNecesidad = precio * (1 - PorcentajeDeDescuento/100);
			
		}
	}
	
	public ProductoDePrimeraNecesidad(String nombre, double precio, boolean precioCuidado, double PorcentajeDeDescuento)
	{
		super(nombre, precio, precioCuidado);
		if(PorcentajeDeDescuento >= 0 && PorcentajeDeDescuento <= 100)
		{
			precioDePrimeraNecesidad = precio * (1 - PorcentajeDeDescuento/100);
			
		}
	}
	
	@Override
	public double getPrecio() 
	{
		return precioDePrimeraNecesidad;
	}
}
