package supermercado;
import java.util.List;
import java.util.ArrayList;

public class Supermercado {
	private String nombre;
	private String direccion;
	public ArrayList<Producto> productos = new ArrayList<Producto>();
	
	public Supermercado(String unNombre, String unaDireccion)
	{
		nombre = unNombre;
		direccion = unaDireccion;
	}
	
	public void agregarProducto(Producto unProducto)
	{
		productos.add(unProducto);
	}
	
	public int getCantidadDeProductos()
	{
		return productos.size();
	}
	
	
	
	public double getPrecioTotal() 
	{
		double total = 0d;
		if(!productos.isEmpty())
		{
			for(Producto producto:productos)
			{
				total += producto.getPrecio();
			}
		}
		return total;
	}

}
