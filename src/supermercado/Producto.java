package supermercado;

public class Producto {
	private String nombre;
	private double precio;
	private boolean preciosCuidados;
	
	
	public Producto(String unNombre, double unPrecio)
	{
		setNombre(unNombre);
		setPrecio(unPrecio);
		setPreciosCuidados(false);
	}
	/**
	 * @param unPrecio se le pasa un precio
	 */
	public Producto(String unNombre, double unPrecio, boolean esPrecioCuidado)
	{
		this(unNombre, unPrecio);
		setPreciosCuidados(esPrecioCuidado);
	}
	
	private void setNombre(String unNombre) 
	{
		nombre = unNombre;
	}
	

	private void setPrecio(double unPrecio)
	{
		precio = unPrecio;
	}
	
	public double getPrecio()
	{
		return precio;
	}
	
	public String getNombre()
	{
		return nombre;
	}
	
	public boolean esPrecioCuidado()
	{
		return preciosCuidados;
	}
	
	private void setPreciosCuidados(boolean precioCuidado)
	{
		preciosCuidados = precioCuidado;
	}
	
	
	public void aumentarPrecio(double aumento)
	{
		double precioAumentado = precio + aumento;
		setPrecio(precioAumentado);
	}
	

}
