package tpCompositeEj01;

import java.util.ArrayList;

public class Campo {

	private ArrayList<Parcela> parcelas = new ArrayList<Parcela>();
	
	public void agregarParcela(Parcela unaParcela)
	{
		this.getParcelas().add(unaParcela);
	}
	
	public void sacarParcela (Parcela unaParcela)
	{
		this.getParcelas().remove(unaParcela);
	}
	
	
	private ArrayList<Parcela> getParcelas()
	{
		return this.parcelas;
	}
	
	
	public float calcularGananciaAnual()
	{
		float gananciaAnual = 0f;
		
		for (Parcela parcela: this.getParcelas())
		{
			gananciaAnual += parcela.calcularGananciaAnual();
		}
		
		return gananciaAnual;
	}
}
