package tpCompositeEj01;

public class Cultivo {
	private String nombre;
	private float gananciaAnual;
	
	
	public Cultivo(String nombre, float gananciaAnual)
	{
		this.setGananciaAnual(gananciaAnual);
		this.setNombre(nombre);
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public float getGananciaAnual() {
		return gananciaAnual;
	}
	public void setGananciaAnual(float gananciaAnual) {
		this.gananciaAnual = gananciaAnual;
	}
	
	
}
