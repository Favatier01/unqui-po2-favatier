package tpCompositeEj01;

import java.util.ArrayList;

public class ParcelaMixta extends Parcela{
	
	private ArrayList<Cultivo> cultivos = new ArrayList<Cultivo>();
	
	
	public void agregarCultivo(Cultivo unCultivo)
	{
		this.getCultivos().add(unCultivo);
	}
	
	public void sacarCultivo(Cultivo unCultivo)
	{
		this.getCultivos().remove(unCultivo);
	}
	


	@Override
	public float calcularGananciaAnual()
	{
		float gananciaTotal = 0f;
		
		for(Cultivo cultivo:this.getCultivos())
		{
			gananciaTotal += cultivo.getGananciaAnual()/this.getCultivos().size();
		}
		
		return gananciaTotal;
	}
	
	private ArrayList<Cultivo> getCultivos()
	{
		return this.cultivos;
	}


}
