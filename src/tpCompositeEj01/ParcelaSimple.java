package tpCompositeEj01;

public class ParcelaSimple extends Parcela{
	
	private Cultivo cultivo;
	
	
	
	public ParcelaSimple(Cultivo cultivo) {
		super();
		this.cultivo = cultivo;
	}


	@Override
	public float calcularGananciaAnual()
	{
		return this.cultivo.getGananciaAnual();
	}
}
