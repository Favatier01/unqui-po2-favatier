package tpCompositeEj01;

import java.util.ArrayList;

public class ParcelaMixtaComposite extends Parcela {

	private ArrayList<Parcela> parcelas = new ArrayList<Parcela>();
	
	
	public void agregarParcela(Parcela unParcela)
	{
		this.getParcelas().add(unParcela);
	}
	
	public void sacarCultivo(Parcela unParcela)
	{
		this.getParcelas().remove(unParcela);
	}
	


	@Override
	public float calcularGananciaAnual()
	{
		float gananciaTotal = 0f;
		
		for(Parcela parcela:this.getParcelas())
		{
			gananciaTotal += parcela.calcularGananciaAnual()/this.getParcelas().size();
		}
		
		return gananciaTotal;
	}
	
	private ArrayList<Parcela> getParcelas()
	{
		return this.parcelas;
	}

}
