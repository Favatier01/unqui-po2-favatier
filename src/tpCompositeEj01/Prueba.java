package tpCompositeEj01;

public class Prueba {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Cultivo soja  = new Cultivo("Soja",  500f);
		Cultivo trigo = new Cultivo("Trigo", 300f);
		
		ParcelaSimple parcela1 = new ParcelaSimple(soja);
		ParcelaSimple parcela2 = new ParcelaSimple(trigo);
		/*
		ParcelaMixta parcela3 = new ParcelaMixta();
		parcela3.agregarCultivo(soja);
		parcela3.agregarCultivo(soja);
		parcela3.agregarCultivo(trigo);
		parcela3.agregarCultivo(trigo);
		*/
		
		ParcelaMixtaComposite parcela3 = new ParcelaMixtaComposite();
		parcela3.agregarParcela(parcela1);
		parcela3.agregarParcela(parcela1);
		parcela3.agregarParcela(parcela2);
		parcela3.agregarParcela(parcela2);
		
		
		ParcelaSimple parcela4 = new ParcelaSimple(soja); 
		
		Campo elCampo = new Campo();
		
		elCampo.agregarParcela(parcela1);
		elCampo.agregarParcela(parcela2);
		elCampo.agregarParcela(parcela3);
		elCampo.agregarParcela(parcela4);
		
		float gananciaAnual = elCampo.calcularGananciaAnual();
		
		System.out.println(gananciaAnual);
		System.out.println(500*2 +300 + 500/2 + 300/2);
		
		
		
	}

}
