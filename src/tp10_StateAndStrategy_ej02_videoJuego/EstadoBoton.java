package tp10_StateAndStrategy_ej02_videoJuego;

public abstract class EstadoBoton {
	
	public abstract String presionarBoton(VideoJuego game); 
}
