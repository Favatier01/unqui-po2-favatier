package tp10_StateAndStrategy_ej02_videoJuego;

public class BotonConFichas extends EstadoBoton{

	@Override
	public String presionarBoton(VideoJuego game) {
		
		//game.iniciarJuego(2);
		game.descontarFicha(2);
		game.jugar();
		return "Juego iniciado para dos jugadores";
	}

}
