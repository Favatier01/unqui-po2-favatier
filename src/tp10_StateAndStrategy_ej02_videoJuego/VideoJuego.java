package tp10_StateAndStrategy_ej02_videoJuego;

import java.util.ArrayList;

//State hace cosas distintas 
public class VideoJuego {
	
	private int cantidadDeFichas;
	private EstadoBoton boton;
	
	//private EstadoBoton estadosPosible[] = EstadoBoton[3];
	
	
	public VideoJuego()//EstadoBoton sinFichas, EstadoBoton unaFicha, EstadoBoton fichas)
	{
		this.setCantidadDeFichas(0);
		boton = new BotonSinFichas();
		//estadosPosible = EstadoBoton[3];
	}
	
	private void setCantidadDeFichas(int cant)
	{
		this.cantidadDeFichas = cant;
	}
	
	private int getCantidadDeFichas()
	{
		return this.cantidadDeFichas;
	}
	
	public void agregarFicha()
	{
		int cantidadTotal = this.getCantidadDeFichas() + 1;
		this.setCantidadDeFichas(cantidadTotal);
		
	
	}
	
	public void descontarFicha(int cant)
	{
		int cantidadTotal = this.getCantidadDeFichas() - cant;
		this.setCantidadDeFichas(cantidadTotal);
		
	
	}
	
	public void actualizarEstadoMaquina()//EstadoBoton estadoSinFichas, EstadoBton)
	{
		int cant = this.getCantidadDeFichas();
		
		if(cant == 0)
		{
			this.setBoton(new BotonSinFichas());
		}
		else if(cant == 1)
		{
			this.setBoton(new BotonConUnaFicha());
		}
		else
		{
			this.setBoton(new BotonConFichas());
		}
	}
	
	public void setBoton(EstadoBoton unEstadoBoton)
	{
			this.boton = unEstadoBoton;
	}
	
	protected EstadoBoton getBoton()
	{
		return this.boton;
	}
	
	public String presionarBoton()
	{
		return this.getBoton().presionarBoton(this);
	}
	
	public void jugar()
	{
		this.setBoton(new BotonJugando());
	}
	public void finalizarJuego()
	{
		this.actualizarEstadoMaquina();
	}
	
	/*
	public String mostrarPantalla()
	{
		return "Pantalla";
	}
	
	public String iniciarJuego(int cantidadDeJugadores)
	{
		if(cantidadDeJugadores == 1)
		{
			return "Juego iniciado para un jugador";
		}
		else
		{
			return "Juego iniciado para dos jugadores";
		}
	}
	*/
}
