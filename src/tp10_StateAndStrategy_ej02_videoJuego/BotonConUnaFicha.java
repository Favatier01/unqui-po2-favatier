package tp10_StateAndStrategy_ej02_videoJuego;

public class BotonConUnaFicha extends EstadoBoton{

	@Override
	public String presionarBoton(VideoJuego game) {
		
		//game.iniciarJuego(1);
		game.descontarFicha(1);
		game.jugar();
		return "Juego iniciado para un jugador";
	}

}
