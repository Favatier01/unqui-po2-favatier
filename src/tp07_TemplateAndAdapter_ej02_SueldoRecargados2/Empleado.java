package tp07_TemplateAndAdapter_ej02_SueldoRecargados2;

public abstract class Empleado {
	

 
	public float calcularSueldo()
	{
		float sueldoBruto = 0f;
		
		sueldoBruto += this.sueldoBasico()+ this.calcularRetribucionPorHorasTrabajadas() +this.calcularRetribucionPorFamilia();
		
		return sueldoBruto - this.calcularDescuentos(sueldoBruto);

	}
	
	public abstract float sueldoBasico();
	
	public abstract float calcularRetribucionPorHorasTrabajadas();
	
	public abstract float calcularRetribucionPorFamilia();
	

	public float calcularDescuentos(float sueldoBruto)
	{
		return 0.13f * sueldoBruto;
	}
	
	
}
