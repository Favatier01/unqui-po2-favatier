package tp07_TemplateAndAdapter_ej02_SueldoRecargados2;

public class Planta  extends Empleado{

	float sueldoBasico;
	float plusSueldoPorCadaHijo;
	int cantidadDeHijos;
	
	public Planta(int cantidadDeHijos)
	{
		this.setSueldoBasico(3000);
		this.setPlusSueldoPorCadaHijo(150);
	}
	
	
	public float getSueldoBasico() {
		return sueldoBasico;
	}

	public void setSueldoBasico(float sueldoBasico) {
		this.sueldoBasico = sueldoBasico;
	}

	public float getPlusSueldoPorCadaHijo() {
		return plusSueldoPorCadaHijo;
	}

	public void setPlusSueldoPorCadaHijo(float plusSueldoPorCadaHijo) {
		this.plusSueldoPorCadaHijo = plusSueldoPorCadaHijo;
	}

	public int getCantidadDeHijos() {
		return cantidadDeHijos;
	}

	public void setCantidadDeHijos(int cantidadDeHijos) {
		this.cantidadDeHijos = cantidadDeHijos;
	}

	@Override
	public float sueldoBasico() {
		
		return this.getSueldoBasico();
	}

	@Override
	public float calcularRetribucionPorHorasTrabajadas() {
	
		return 0;
	}

	@Override
	public float calcularRetribucionPorFamilia() {
		
		return this.getCantidadDeHijos() * this.getPlusSueldoPorCadaHijo();
	}

}
