package tp07_TemplateAndAdapter_ej02_SueldoRecargados2;

public class Temporario extends Empleado{

	float sueldoBasico;
	float sueldoPorHora;
	int horasTrabajadas;
	float sueldoPorFamilia;
	
	
	
	/**
	 * 
	 * @param familia Es true si tiene hijos o esta casado.
	 */
	public Temporario(boolean familia)
	{
		this.setSueldoBasico(1000);
		this.setSueldoPorHora(5);
		this.setFamilia(familia);
		this.setSueldoPorFamilia(100);
	}
	
	
	public float getSueldoPorFamilia() {
		return sueldoPorFamilia;
	}



	public void setSueldoPorFamilia(float sueldoPorFamilia) {
		this.sueldoPorFamilia = sueldoPorFamilia;
	}



	public int getHorasTrabajadas() {
		return horasTrabajadas;
	}



	public void setHorasTrabajadas(int horasTrabajadas) {
		this.horasTrabajadas = horasTrabajadas;
	}

	boolean familia;
	

	
	
	
	public float getSueldoBasico() {
		return sueldoBasico;
	}



	public void setSueldoBasico(float sueldoBasico) {
		this.sueldoBasico = sueldoBasico;
	}



	public float getSueldoPorHora() {
		return sueldoPorHora;
	}



	public void setSueldoPorHora(float sueldoPorHora) {
		this.sueldoPorHora = sueldoPorHora;
	}



	public boolean isFamilia() {
		return familia;
	}



	public void setFamilia(boolean familia) {
		this.familia = familia;
	}



	@Override
	public float sueldoBasico() {
		
		return this.getSueldoBasico();
	}

	@Override
	public float calcularRetribucionPorHorasTrabajadas() {
		return this.getHorasTrabajadas() * this.getSueldoPorHora();
	}

	@Override
	public float calcularRetribucionPorFamilia() {
		
		float sueldoPorFamilia= 0f;
		
		if(this.isFamilia())
		{
			sueldoPorFamilia = this.getSueldoPorFamilia();
		}
		
		return sueldoPorFamilia;
	}
	

}
