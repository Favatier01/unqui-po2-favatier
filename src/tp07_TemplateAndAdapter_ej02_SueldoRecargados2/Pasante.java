package tp07_TemplateAndAdapter_ej02_SueldoRecargados2;

public class Pasante  extends Empleado{

	float sueldoPorHora;
	int horasTrabajadas;
	
	public Pasante()
	{
		this.setSueldoPorHora(40);
	}

	public float getSueldoPorHora() {
		return sueldoPorHora;
	}

	public void setSueldoPorHora(float sueldoPorHora) {
		this.sueldoPorHora = sueldoPorHora;
	}

	public int getHorasTrabajadas() {
		return horasTrabajadas;
	}

	public void setHorasTrabajadas(int horasTrabajadas) {
		this.horasTrabajadas = horasTrabajadas;
	}

	@Override
	public float sueldoBasico() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float calcularRetribucionPorHorasTrabajadas() {
		
		return this.getSueldoPorHora() * this.getHorasTrabajadas();
	}

	@Override
	public float calcularRetribucionPorFamilia() {
		
		return 0;
	}
	
	
	
}
