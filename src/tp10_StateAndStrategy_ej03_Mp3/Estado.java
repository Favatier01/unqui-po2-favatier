package tp10_StateAndStrategy_ej03_Mp3;

public abstract class Estado {

	public abstract String botonPlay (Reproductor mp3);
	public abstract String botonPause(Reproductor mp3);
	public abstract String botonStop (Reproductor mp3);
	
}
