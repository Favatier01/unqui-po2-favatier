package tp10_StateAndStrategy_ej03_Mp3;

public class Song {

	public String play()
	{
		return "Reproduciendo: El vals del duende";
	}
	
	public String pause()
	{
		return "En Pausa: El vals del duende";
	}
	
	public String Stop()
	{
		return "Stop: El vals del duende";
	}
}
