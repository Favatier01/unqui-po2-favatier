package tp10_StateAndStrategy_ej03_Mp3;

public class SeleccionDeCancion extends Estado{

	@Override
	public String botonPlay(Reproductor mp3) {
		
		String cancion =mp3.getActualSong().play();
		mp3.setEstado(new Reproduciendo());
		
		return cancion;
	}

	@Override
	public String botonPause(Reproductor mp3) {
			
		throw new IllegalArgumentException("El boton no tiene funcionalidad en el modo actual");
	}

	@Override
	public String botonStop(Reproductor mp3) {
		
		return "Empty";
	}

}
