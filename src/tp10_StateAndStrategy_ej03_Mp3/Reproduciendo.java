package tp10_StateAndStrategy_ej03_Mp3;

public class Reproduciendo extends Estado{

	@Override
	public String botonPlay(Reproductor mp3) {
		throw new IllegalArgumentException("El boton no tiene funcionalidad en el modo actual");
	}

	@Override
	public String botonPause(Reproductor mp3) {
		
		String cancion = mp3.getActualSong().pause();
		mp3.setEstado(new Pausado());
		return cancion;
	}

	@Override
	public String botonStop(Reproductor mp3) {
		
		String cancion = mp3.getActualSong().Stop();
		mp3.setEstado(new SeleccionDeCancion());
		return cancion;
	}

}
