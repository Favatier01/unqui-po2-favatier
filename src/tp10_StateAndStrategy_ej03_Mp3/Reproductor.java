package tp10_StateAndStrategy_ej03_Mp3;

import java.util.ArrayList;

public class Reproductor {

	ArrayList<Song> listaDeCanciones;
	Estado estado;
	
	public Reproductor(Estado unEstado)
	{
		this.listaDeCanciones = new ArrayList<Song>();
		this.setEstado(unEstado);
	}
	
	public void setEstado(Estado unEstado)
	{
		this.estado = unEstado;
	}
	
	public void agregarCancion(Song unaCancion)
	{
		this.getListaDeCanciones().add(unaCancion);
	}
	
	public ArrayList<Song> getListaDeCanciones()
	{
		return this.listaDeCanciones;
	}
	
	public Song getActualSong()
	{
		return this.getListaDeCanciones().get(0);
	}
	
	public String botonPlay()
	{
		return this.getEstado().botonPlay(this);
	}
	
	public String botonPause()
	{
		return this.getEstado(). botonPause(this);
	}
	
	public String botonStop()
	{
		return this.getEstado().botonStop(this);
	}
	
	public Estado getEstado()
	{
		return this.estado;
	}
}
