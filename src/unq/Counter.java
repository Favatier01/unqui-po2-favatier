package unq;

//import java.util.List;
import java.util.ArrayList;





public class Counter {

	
	private ArrayList<Integer> numeros;
	
	public Counter() {
	this.numeros = new ArrayList<Integer>();
	}
	
	
	
	public int contarPares() {
		return contarMultiplosDe(2);
	}
	
	
	
	public int contarMultiplosDe(int div) {
		
		int cantDeCoincidencias = 0;
		//for (Integer numero  : numeros) modo alternativo
		for (int i = 0; i <numeros.size(); i++)
		{
			if ((numeros.get(i) % div) == 0 )
			{
				cantDeCoincidencias ++;
			}
		}
		
		return cantDeCoincidencias;
	}
	
	
	
	public int contarImpares() {
		int cantImpares = 0;
		for (int i = 0; i <numeros.size(); i++)
		{
			if ((numeros.get(i) % 2) != 0 )
			{
				cantImpares ++;
			}
		}
		
		return cantImpares;
	}
	
	
	
	
	public void addNum(int num) {
		
		numeros.add(num);
	}

}
