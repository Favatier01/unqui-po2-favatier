package unq;

public class MyMainClass {
	 public static void main(String[] args) {
		 /* Ejercicio 2 y 3 me parece
		 Stringues prueba = new Stringues();
		 
		 //da 3
		 System.out.println("1) a.length: " + prueba.a.length());
		 
		 //da error
		 //System.out.println("t.length: " + prueba.t.length()); 
		 
		 //da 1abc porque java convierte todo a string
		 System.out.println("2) 1 + a: " + 1 + prueba.a);
		 
		 //da ABC
		 System.out.println("3) a.toUpperCase(): " + prueba.a.toUpperCase());
		 
		 //da 4
		 System.out.println("4) Libertad.indexOf(r): " + "Libertad".indexOf("r"));
		 
		 //da 7
		 System.out.println("5) Universidad.lastIndexOf(i): " + "Universidad".lastIndexOf("i"));
		 
		 
		 //da il
		 System.out.println("6) Quilmes.substring(2,4) : " + "Quilmes".substring(2,4));
		 
		 //da 3abc
		 System.out.println("7) a.length() + a :" + prueba.a.length() + prueba.a);
		 
		 //false
		 System.out.println("8) (a.length() + a).startsWith(a) :" + (prueba.a.length() + prueba.a).startsWith("a"));
		 
		//da true
		 System.out.println("9) s == a :" + (prueba.s == prueba.a));
		 
		 //true
		 System.out.println("10) a(1,3) equals bc :" + prueba.a.substring(1,3).equals("bc"));
		 
		 //error variable sin inicializar
		 //System.out.println("11) valor por defecto int x: " +  prueba.valorPorDefecto());
		 */
		 
		 Multioperador multi = new Multioperador();
		 multi.addNumbersFromTo(1, 4);
		 multi.printAll();
		 //System.out.println(multi.list.get(1));
		 System.out.println("sumatoria: " + multi.sumAll());
		 System.out.println("productoria: " + multi.productoria());
		 System.out.println("restoria: " + multi.restoria());
		 
	 }
	}
