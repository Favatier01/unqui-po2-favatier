package unq;
import java.util.ArrayList;

public class Multioperador {
	
	public ArrayList<Integer> list = new ArrayList<Integer>();
	
	public void  addNumbersFromTo(int FromNumber, int ToNumber)
	{
		for(int i = FromNumber; i <= ToNumber; i++ )
		{
			list.add(i);
		}
		
	}
	
	public void printAll() 
	{
		for(int i = 0; i < list.size(); i++) 
		{
			System.out.println(list.get(i));
		}
	}
	
	public int sumAll()
	{
		Integer Sum = 0;
		for(Integer Number:list)
		{
			Sum += Number;
		}
		return Sum;
	}
	
	
	public int productoria()
	{
		int Prod = 1;
		for(Integer Number:list)
		{
			Prod = Prod * Number;
		}
		return Prod;
	}
	
	public Integer restoria()
	{
		Integer restoria = 0;
		for(int number:list)
		{
			restoria -= number;
		}
		return restoria;
	}

}
