package tpCompositeEj02;



public class prueba {

	public static void main(String[] args) {

		ShapeShifter a = new LeafElement(1);
		ShapeShifter b = new LeafElement(2);
		
		ShapeShifter c = a.compose(b);
		System.out.println("deep a " + a.deepest());
		
		
		System.out.println("Prueba 1. Compose a two leafs && deepest");
		a.printAll();
		b.printAll();
		c.printAll();
		System.out.println();
		System.out.println("deep a: " + a.deepest());
		System.out.println("deep b: " + b.deepest());
		System.out.println("deepest c: " + c.deepest());
		System.out.println();
		
		System.out.println("Prueba 2. compose a leaf with a composite && deepest");
		
		ShapeShifter d = new LeafElement(3);
		ShapeShifter  d2 = d.compose(c);
		d2.printAll();
		System.out.println();
		System.out.println("deepest d2: " + d2.deepest());
		System.out.println();
		
		System.out.println("Prueba 3. Compose two composite && deepest");
		ShapeShifter a5 = new LeafElement(5);
		ShapeShifter a6 = new LeafElement(6);
		
		ShapeShifter e = a5.compose(a6);
		ShapeShifter f = d2.compose(e);
		System.out.println();
		System.out.println("deepest f: " + f.deepest());
		f.printAll();
		System.out.println();
		System.out.println();
		
		System.out.println("Prueba 4. Flat a shapeShifter && deepest");
		ShapeShifter flat = f.flat();
		flat.printAll();
		System.out.println();
		System.out.println("deepest: " + flat.deepest());

	}

}
