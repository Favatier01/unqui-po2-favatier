package tpCompositeEj02;


import java.util.List;

public abstract class ShapeShifter  {
	
	
	public abstract ShapeShifter compose (ShapeShifter aShape);

	public abstract int deepest();
		
	public abstract ShapeShifter flat();

	public abstract List<Integer> values();
	  
	public abstract void printAll();
	
	public abstract void increaseDeep();
		
}
