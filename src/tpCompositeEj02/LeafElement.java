package tpCompositeEj02;

import java.util.ArrayList;
import java.util.List;

public class LeafElement extends ShapeShifter{

	private Integer element;
	private int deep = 0;
	
	public LeafElement (Integer aNumber)
	{
		this.element = aNumber;
	}
	

	
	@Override
	public List<Integer> values() 
	{
		List<Integer> elementInAList = new ArrayList<Integer>();
		elementInAList.add(this.element);
		
		return elementInAList;
	}
	
	
	
	@Override
	public ShapeShifter compose (ShapeShifter aShape){
			
			CompositeElement shape =  new CompositeElement();
			shape.add(this);
			shape.add(aShape);
			shape.increaseDeep();
			return shape;
		}

	@Override
	public void printAll() {
		
		System.out.print(this.element + " ");
	}
	
	@Override
	public void increaseDeep()
	{
		this.deep += 1;
	}
	
	@Override
	public int deepest()
	{
		return this.deep;
	}

	@Override
	public ShapeShifter flat() {
		// TODO Auto-generated method stub
		return null;
	}


}
