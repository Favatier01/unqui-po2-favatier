package tpCompositeEj02;

import java.util.List;

public interface IShapeShifter {

	public IShapeShifter compose (IShapeShifter aShape);
	public int deepest(IShapeShifter aShape);
	public IShapeShifter flat();
	public List<Integer> values();
}
