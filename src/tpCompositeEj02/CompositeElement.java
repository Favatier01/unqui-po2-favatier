package tpCompositeEj02;

import java.util.ArrayList;
import java.util.List;

public class CompositeElement extends ShapeShifter{
	
	ArrayList<ShapeShifter> elements =  new ArrayList<ShapeShifter>();

	
	
	@Override
	public ShapeShifter compose (ShapeShifter aShape)
	{
		
		CompositeElement newShape = new CompositeElement();
		
		newShape.add(this);
		newShape.add(aShape);
		newShape.increaseDeep();
		
		return  newShape;
		
	}
	
	
	@Override
	public void increaseDeep()
	{
		for(ShapeShifter i: this.getElements())
		{
			i.increaseDeep();
		}
	}
	
	@Override
	public int deepest()
	{
		int maxDeep = 1;
		
		for (ShapeShifter i:this.getElements())
		{
			if(i.deepest()> maxDeep)
			{
				maxDeep = i.deepest();
			}
		}
		
		return maxDeep;
	}
	
	
	
	public  void add(ShapeShifter aShape)
	{
		this.getElements().add(aShape);
	}
	
	
	
	private ArrayList<ShapeShifter> getElements()
	{
		return this.elements;
	}
	


	

	
	
	
	@Override
	public ShapeShifter flat() {
		
		CompositeElement shapeFlated = new CompositeElement();
		shapeFlated.add(this.values());
		shapeFlated.increaseDeep();
		
		return shapeFlated;
	}
	
	private void add (List<Integer> aList)
	{
		this.setElements(aList);
	}
	
	private void setElements(List<Integer> aList)
	{
		for(Integer i:aList)
		{
			LeafElement leaf = new LeafElement(i);
			
			this.getElements().add(leaf);
		}
	}
	
	 @Override
	public List<Integer> values()
	 {
		 List<Integer> values = new ArrayList<Integer>(); 
		 
		 for(ShapeShifter i: this.getElements())
		 {
			 values.addAll(i.values());
		 }
		 
		 return values;
	 }
	 
	 @Override
	 public void printAll()
	 {	
		 System.out.print("[");
		 for(ShapeShifter i:this.getElements())
		 {
			 for(int index = 1; index == this.getElements().size(); index ++)
			 {  
				 	if(index != 1)
				 	{
				 		System.out.print("[ ");
				 	}
			 }
			
			 i.printAll();
			 
			 
			 for(int index = 1; index == this.getElements().size(); index ++)
			 {  
				 	if(index != 1)
				 	{
				 		System.out.print(" ]");
				 	}
			 }
			 
		 }
		 System.out.print("]");
	 }
	 

	 
}
 