package tp07_TemplateAndAdapter_ej08_Fechas;

import java.time.LocalDate;

import org.joda.time.DateTime;

public class FechaAdapter implements IFecha{
	
	//LocalDate(int year, int monthOfYear, int dayOfMonth)
	DateTime dateTime;
	
	
	public FechaAdapter(DateTime fecha)
	{
		this.dateTime = fecha;

	}
	
	public DateTime getDateTime()
	{
		return this.dateTime;
	}
	
	
	
	@Override
	public boolean antesDeAhora() {
		
		return this.dateTime.isBeforeNow();
	}

	@Override
	public boolean antesDe(IFecha fecha) {

		FechaAdapter castFecha = (FechaAdapter) fecha;
		return this.getDateTime().isBefore(castFecha.getDateTime());
	}

	@Override
	public boolean despuesDeAhora() {
		return this.getDateTime().isAfterNow();
	}

	@Override
	public boolean despuesDeFecha(IFecha fecha) {
		FechaAdapter castFecha = (FechaAdapter) fecha;
		return this.getDateTime().isAfter(castFecha.getDateTime());
	}

	@Override
	public int dia() {
		return this.getDateTime().getDayOfMonth();
	}

	@Override
	public int mes() {
		return this.getDateTime().monthOfYear().get();
	}

	@Override
	public int anio() {
		return this.getDateTime().year().get();
	}

	@Override
	public IFecha restarDias(int dias) {
		
		FechaAdapter nuevaFecha = new FechaAdapter(this.getDateTime().minusDays(dias));
		return nuevaFecha;
	}

}
