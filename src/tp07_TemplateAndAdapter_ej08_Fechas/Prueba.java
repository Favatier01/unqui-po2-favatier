package tp07_TemplateAndAdapter_ej08_Fechas;

import java.time.LocalDate;

import org.joda.time.DateTime;

public class Prueba {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		FechaAdapter a = new FechaAdapter(new DateTime(2020, 12, 24, 00, 00));
		
		IFecha b = (IFecha) a;
		System.out.println(b.dia());
		System.out.println(b.mes()); 
		System.out.println(b.anio());
		
		
		
		System.out.println(b.antesDe(new FechaAdapter(new DateTime(2020, 12, 25, 00, 00))));
		System.out.println(b.despuesDeFecha(new FechaAdapter(new DateTime(2020, 12, 25, 00, 00))));
		System.out.println(b.antesDeAhora());
		System.out.println(b.despuesDeAhora());
		System.out.print((b.restarDias(1)).dia() + " " + (b.restarDias(1)).mes() + " " + (b.restarDias(1)).anio() );
		
		
	}

}
