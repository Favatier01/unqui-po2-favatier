package tp07_TemplateAndAdapter_ej02_SueldoRecargados;

public class Temporario extends Empleado{

	boolean estaCasado;
	
	/**
	 * 
	 * @param sueldoBasico = valor segun ejercicio 1000$
	 * @param sueldoPorHoras = valor segun ejercicio 5$
	 * @param cantidadDeHijos
	 * @param estaCasado
	 */
	public Temporario(float sueldoBasico, float sueldoPorHoras, int cantidadDeHijos, boolean estaCasado) {
		super(sueldoBasico, sueldoPorHoras, cantidadDeHijos);
		this.estaCasado = estaCasado;
		// TODO Auto-generated constructor stub
	}





	@Override
	public float calcularRetribucionPorFamilia() {

		float sueldoPorFamilia = 0f;
		
		if(super.getCantidadDeHijos() != 0 || this.estaCasado)
		{
			sueldoPorFamilia = 100;
		}
		
		return sueldoPorFamilia;
	}

}
