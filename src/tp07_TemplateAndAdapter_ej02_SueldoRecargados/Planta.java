package tp07_TemplateAndAdapter_ej02_SueldoRecargados;

public class Planta extends Empleado{

	float sueldoPorCadaHijo;
	
	public float getSueldoPorCadaHijo() {
		return sueldoPorCadaHijo;
	}

	public void setSueldoPorCadaHijo(float sueldoPorCadaHijo) {
		this.sueldoPorCadaHijo = sueldoPorCadaHijo;
	}

	/**
	 * 
	 * @param sueldoBasico = valor segun ejercicio 3000
	 * @param cantidadDeHijos
	 * @param sueldoPorCadaHijo = valor segun ejercicio 150
	 */
	public Planta(float sueldoBasico, float sueldoPorCadaHijo, int cantidadDeHijos) {
		super(sueldoBasico, 0, cantidadDeHijos);
		// TODO Auto-generated constructor stub
		this.sueldoPorCadaHijo = sueldoPorCadaHijo;
	}

	@Override
	public float calcularRetribucionPorFamilia() {
		
		return this.getSueldoPorCadaHijo() * super.getCantidadDeHijos();
	}

}
