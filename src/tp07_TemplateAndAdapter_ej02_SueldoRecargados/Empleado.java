package tp07_TemplateAndAdapter_ej02_SueldoRecargados;

public abstract class Empleado {
	
	
	private float sueldoBasico;
	private int horasTrabajadas;
	private int cantidadDeHijos;
	private float sueldoPorHora;

	public float getSueldoPorHora() {
		return sueldoPorHora;
	}

	public void setSueldoPorHora(float sueldoPorHora) {
		this.sueldoPorHora = sueldoPorHora;
	}

	public Empleado(float sueldoBasico, float sueldoPorHoras, int cantidadDeHijos) {
		super();
		this.sueldoBasico = sueldoBasico;
		this.cantidadDeHijos = cantidadDeHijos;
		this.sueldoPorHora = sueldoPorHoras;
	}

	public float getSueldoBasico() {
		return sueldoBasico;
	}
	
	public void setSueldoBasico(float sueldoBasico) {
		this.sueldoBasico = sueldoBasico;
	}
	
	public int getHorasTrabajadas() {
		return horasTrabajadas;
	}
	
	public void setHorasTrabajadas(int horasTrabajadas) {
		this.horasTrabajadas = horasTrabajadas;
	}
	
	public int getCantidadDeHijos() {
		return cantidadDeHijos;
	}
	
	public void setCantidadDeHijos(int cantidadDeHijos) {
		this.cantidadDeHijos = cantidadDeHijos;
	}
	
	public float calcularSueldo()
	{
		float sueldoBruto = 0f;
		
		sueldoBruto += this.getSueldoBasico()+ this.calcularRetribucionPorHorasTrabajadas() +this.calcularRetribucionPorFamilia();
		
		return sueldoBruto - this.calcularDescuentos(sueldoBruto);

	}
	
	public float calcularRetribucionPorHorasTrabajadas()
	{
		return this.getSueldoPorHora()*this.getHorasTrabajadas();
	}
	
	public abstract float calcularRetribucionPorFamilia();

	public float calcularDescuentos(float sueldoBruto)
	{
		return 0.13f * sueldoBruto;
	}
	
	
}
